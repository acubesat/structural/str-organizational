# Welcome to STR gitlab organizational (under construction version)
To-Do list:

- [x] Separate technical issues in different repos
- [x] Create repos for :
   * OBC/ADCS campaign tasks
   * ADM campaign tasks
   * CADs (separate subfolders for each AcubeSAT subassembly 
     & create as more subassemblies as possible for quicker navigation.
     No parts should be uploaded since by opening an assembly file and
     saving it, a CAD program automatically creates part files). All
     files should be .step for them to be opened globally. 
   * STLs repo for visualization
   * Printing
   * Theoretical background/documentation
   * Manufacturing/procurements
- [x] Migrate issues unrelated to organizational to their respective repos
- [ ] Create gitlab organizational issues
- [ ] Adapt the labeling system to changes for easier board organization