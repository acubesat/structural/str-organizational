Workshop
-------------------------------------------
- (εγιναν δοκιμες για το σχεδιο, του Διαμαντη του πηρε χρονο, αλλα νομιζει πως δεν θα παρει πανω απο 1.5 ωρα)
	αν δεν φτανει ο χρονος, θα βγει το πλαϊνο frame
	θα γινει τεστ Βασιλης + Κικας, και μετα θα βγει plan of actions
- Δεν θα δειξουμε πως γινεται η αναλυση, μονο τα αποτελεσματα της
- Ο Δημητρης θα λειπει (Βελγιο για campaign), οποτε το αναλαμβανει κυριως ο Βασιλης (κυριως ασχολουμενος)
- Ο Αγγελος ειχε ξεκινησει την παρουσιαση

-------------------------------------------

Εχει να γινει το design του main body

Στελλα
	περιμενουνε τα feedthroughs (?)
	no updates on the container
Γιωργος
	break απο την ομαδα
		το leak detection test θα πρεπει να το αναλαβει καποιος αλλος

---------------------------------

# Issues

considerations
	οσα πρεπει να λαβουμε υποψιν οσο σχεδιαζουμε πως θα αντιμετωπισουμε το προβλημα

steps
	πρεπει να πραγματοποιούνται διαδοχικα, ή εστω να υπαρχει μια συνεχεια

-------------------------------------------

Ο Διαμαντης αναλαμβανει το leak test
	να κανει την πατεντα για τον ανταπτορα που θα ελεγχει την παροχη του Helium απο τη φυαλη

Το tank θα το παρει ο Δημητρης συντομα


-------------------------------------------

payload - vibe adaptor
	θα χρειαστει technical session
	(απουσια Αγγελου, δεν θα το συζητησουμε σημερα)
	πολυ μεγαλη υποθεση

-------------------------------------------

availability καθενος για εαρινο εξαμηνο

Στελλα
	Δεν εχει κατι στανταρ, αλλαζει ανα βδομαδα

Βασιλης (οπως το βλεπει τωρα)
	Πριν το Πασχα 6 ωρες τη βδομαδα μαξ
	Μετα το Πασχα θα μπορει να αφιερωσει πολυ περισσοτερο χρονο τη βδομαδα

SU
	μηνιαιο expected availability για να συγχρονιζονται καλυτερα
